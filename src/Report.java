import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Report {
    private JButton tempuraButton;
    private JButton ramenButton;
    private JButton udonButton;
    private JTextArea text1;
    private JLabel text2;
    private JPanel root;
    private JButton sobaButton;
    private JButton riceButton;
    private JButton beerButton;
    private JButton checkoutButton;
    private JTextField couponField1;
    private JButton couponUseButton;
    private int price = 0;
    private int total = 0;
    String couponCode = "1";
    int count = 0;
    String[] options = {"S","M","L"};


    void order(String foodName){
        int confirmation = JOptionPane.showConfirmDialog(null,
                "Would you like to order " + foodName,
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION);
        if (foodName == "Tempra") {
            price = 800;
        } else if(foodName == "Udon") {
            price = 600;
        } else if(foodName == "Rice") {
            price = 300;
        } else if(foodName == "Beer") {
            price = 400;
        } else if(foodName == "Soba") {
            price = 500;
        } else if (foodName == "Ramen S") {
            price = 450;
        } else if (foodName == "Ramen M") {
            price = 700;
        } else if (foodName == "Ramen L") {
            price = 750;
        }



        if (confirmation == 0) {
            JOptionPane.showMessageDialog(null,"Order for " + foodName + " received");
            String currentText = text1.getText();
            text1.setText(currentText + foodName + " \u00a5" + price + "\n");
            total += price;
            text2.setText("Total: " + total + " yen");
        }
    }

    public Report() {
        text2.setText("Total: 0 yen");
        ramenButton.setIcon(new ImageIcon(
                this.getClass().getResource("ramen.png")
        ));
        sobaButton.setIcon(new ImageIcon(
                this.getClass().getResource("soba.png")
        ));
        riceButton.setIcon(new ImageIcon(
                this.getClass().getResource("rice.png")
        ));
        udonButton.setIcon(new ImageIcon(
                this.getClass().getResource("udon.png")
        ));
        beerButton.setIcon(new ImageIcon(
                this.getClass().getResource("beer.png")
        ));
        tempuraButton.setIcon(new ImageIcon(
                this.getClass().getResource("tempura.png")
        ));


        tempuraButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Tempra");
            }
        });
        ramenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int size = JOptionPane.showOptionDialog(null,
                        "What size do you want it to be?",
                        "Size",
                        JOptionPane.YES_NO_CANCEL_OPTION,
                        JOptionPane.QUESTION_MESSAGE,
                        null,
                        options,
                        options[2]);
                switch (size) {
                    case JOptionPane.YES_OPTION:
                        order("Ramen S");
                        break;
                    case JOptionPane.NO_OPTION:
                        order("Ramen M");
                        break;
                    case JOptionPane.CANCEL_OPTION:
                        order("Ramen L");
                        break;
                }
            }
        });
        udonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Udon");
            }
        });
        beerButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Beer");
            }
        });
        sobaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Soba");
            }
        });
        riceButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Rice");
            }
        });


        checkoutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(null,
                        "Would you like to check out?",
                        "Checkout Confirmation",
                        JOptionPane.YES_NO_OPTION
                );

                if (confirmation == 0) {
                    JOptionPane.showMessageDialog(null,
                            "The total price is " + total + " yen");
                    text1.setText("");
                    total = 0;
                    text2.setText("Total: " + total + " yen");
                    count = 0;
                }
            }
        });

        couponUseButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(null,
                        "Do you want to use a this coupon?",
                        "",JOptionPane.YES_NO_OPTION);

                String confirmation2 = couponField1.getText();

                if (confirmation == 0) {
                    if (confirmation2.equals(couponCode)) {
                        if (count == 0) {
                            if (total >= 300){
                                total = total - 300;
                                text2.setText("Total: " + total + " yen");
                                String currentText = text1.getText();
                                text1.setText(currentText + "Coupon" + " -\u00a5300\n");
                                couponField1.setText("");
                                count = count + 1;
                            }
                            else {
                                JOptionPane.showMessageDialog(null,
                                        "Choose food ahead");
                                couponField1.setText("");
                            }
                        }else {
                            JOptionPane.showMessageDialog(null,
                                    "This coupon has been used");
                            couponField1.setText("");
                        }

                    }else{
                        JOptionPane.showMessageDialog(null,
                                "The number is different.");
                        couponField1.setText("");
                    }

                }
            }
        });

    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("experiment");
        frame.setContentPane(new Report().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
